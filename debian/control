Source: hyphy
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>,
           Étienne Mollier <emollier@debian.org>,
           Nilesh Patra <nilesh@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               cmake,
               mpi-default-dev,
               libcurl4-gnutls-dev | libcurl4-dev,
               libssl-dev,
               libsqlite3-dev,
               pkgconf,
               libpython3-all-dev,
               python3-all-dev:any,
               chrpath
Standards-Version: 4.7.2
Vcs-Browser: https://salsa.debian.org/med-team/hyphy
Vcs-Git: https://salsa.debian.org/med-team/hyphy.git
Homepage: https://hyphy.org/
Rules-Requires-Root: no

Package: hyphy-pt
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         hyphy-common (= ${source:Version})
Description: Hypothesis testing using Phylogenies (pthreads version)
 HyPhy is an open-source software package for the analysis of genetic
 sequences using techniques in phylogenetics, molecular evolution, and
 machine learning. It features a complete graphical user interface (GUI)
 and a rich scripting language for limitless customization of analyses.
 Additionally, HyPhy features support for parallel computing environments
 (via message passing interface) and it can be compiled as a shared
 library and called from other programming environments such as Python or
 R.  Continued development of HyPhy is currently supported in part by an
 NIGMS R01 award 1R01GM093939.
 .
 This package provides an executable using pthreads to do multiprocessing.

Package: hyphy-mpi
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         hyphy-common (= ${source:Version})
Description: Hypothesis testing using Phylogenies (MPI version)
 HyPhy is an open-source software package for the analysis of genetic
 sequences using techniques in phylogenetics, molecular evolution, and
 machine learning. It features a complete graphical user interface (GUI)
 and a rich scripting language for limitless customization of analyses.
 Additionally, HyPhy features support for parallel computing environments
 (via message passing interface) and it can be compiled as a shared
 library and called from other programming environments such as Python or
 R.  Continued development of HyPhy is currently supported in part by an
 NIGMS R01 award 1R01GM093939.
 .
 This package provides an executable using MPI to do multiprocessing.

Package: hyphy-common
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Suggests: hyphy-mpi | hyphy-pt
Description: Hypothesis testing using Phylogenies (common files)
 HyPhy is an open-source software package for the analysis of genetic
 sequences using techniques in phylogenetics, molecular evolution, and
 machine learning. It features a complete graphical user interface (GUI)
 and a rich scripting language for limitless customization of analyses.
 Additionally, HyPhy features support for parallel computing environments
 (via message passing interface) and it can be compiled as a shared
 library and called from other programming environments such as Python or
 R.  Continued development of HyPhy is currently supported in part by an
 NIGMS R01 award 1R01GM093939.
 .
 This package provides files that are common to all binary versions.
